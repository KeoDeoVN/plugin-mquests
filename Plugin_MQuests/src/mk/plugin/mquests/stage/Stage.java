package mk.plugin.mquests.stage;

import java.util.List;
import java.util.Map;

public class Stage {
	
	private StageType type;
	private int count;
	private Map<String, List<String>> data;
	
	public Stage(StageType type, int count, Map<String, List<String>> data) {
		this.type = type;
		this.count = count;
		this.data = data;
	}
	
	public StageType getType() {
		return this.type;
	}
	
	public int getCountMax() {
		return this.count;
	}
	
	public Map<String, List<String>> getData() {
		return this.data;
	}
	
}
