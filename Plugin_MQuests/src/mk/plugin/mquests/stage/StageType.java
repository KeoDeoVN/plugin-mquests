package mk.plugin.mquests.stage;

import java.util.List;

import com.google.common.collect.Lists;

public enum StageType {
	
	BLOCK_BREAK(Lists.newArrayList("item-data")),
	BLOCK_PLACE(Lists.newArrayList("item-data")),
	ENTITY_KILL(Lists.newArrayList("entity-type")),
	TIME_PLAY(Lists.newArrayList()),
	MYTHICMOB_KILL(Lists.newArrayList("mythicmob-id")),
	FISHING(Lists.newArrayList("item-data")),
	DEATH(Lists.newArrayList()),
	
	;
	
	private List<String> dataRequired;
	
	private StageType(List<String> dataRequired) {
		this.dataRequired = dataRequired; 
	}
	
	public List<String> getDataRequired() {
		return this.dataRequired;
	}
	
}
