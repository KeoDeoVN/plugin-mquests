package mk.plugin.mquests.block;

import org.bukkit.Material;

public class ItemData {
	
	private Material material;
	private int durability;
	
	public ItemData(Material material, int durability) {
		this.material = material;
		this.durability = durability;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public int getDurability() {
		return this.durability;
	}
	
}
