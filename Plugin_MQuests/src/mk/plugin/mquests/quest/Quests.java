package mk.plugin.mquests.quest;

import java.util.Map;

import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

public class Quests {
	
	private static Map<String, Quest> quests = Maps.newHashMap();
	private static Map<String, Quester> questers = Maps.newHashMap();
	
	public Quest getQuest(String id) {
		return quests.get(id);
	}
	
	public Quester getQuester(String name) {
		return questers.get(name);
	}
	
	public Quester getQuester(Player player) {
		return questers.get(player.getName());
	}
	
	public void setQuester(String name, Quester quester) {
		questers.put(name, quester);
	}
	
	public void setQuester(Player player, Quester quester) {
		setQuester(player.getName(), quester);
	}
	
}
