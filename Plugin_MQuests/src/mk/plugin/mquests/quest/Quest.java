package mk.plugin.mquests.quest;

import java.util.List;

import mk.plugin.mquests.stage.Stage;

public class Quest {
	
	private String name;
	private long cooldown;
	private List<Stage> stages;
	private Difficulty difficulty;
	
	public Quest(String name, long cooldown, List<Stage> stages, Difficulty difficulty) {
		this.name = name;
		this.cooldown = cooldown;
		this.stages = stages;
		this.difficulty = difficulty;
	}
	
	public String getName() {
		return this.name;
	}
	
	public long getCooldown() {
		return this.cooldown;
	}
	
	public List<Stage> getStages() {
		return this.stages;
	}
	
	public Difficulty getDifficulty() {
		return this.difficulty;
	}
	
	
}
