package mk.plugin.mquests.quest;

import java.util.Map;

public class Quester {
	
	private Map<String, QuestData> data;
	
	public Quester(Map<String, QuestData> data) {
		this.data = data;
	}
	
	public Map<String, QuestData> getData() {
		return this.data;
	}
	
}
