package mk.plugin.mquests.quest;

public class QuestData {
	
	private String id;
	private int currentStage;
	
	public QuestData(String id, int currentStage) {
		this.id = id;
		this.currentStage = currentStage;
	}
	
	public String getID() {
		return this.id;
	}
	
	public int getCurrentStage() {
		return this.currentStage;
	}
	
}
