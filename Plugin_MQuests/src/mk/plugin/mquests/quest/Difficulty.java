package mk.plugin.mquests.quest;

public enum Difficulty {
	
	EASY(1),
	NORMAL(2),
	HARD(3);
	
	private int rate;
	
	private Difficulty(int rate) {
		this.rate = rate;
	}
	
	public int getRate() {
		return this.rate;
	}
	
}
